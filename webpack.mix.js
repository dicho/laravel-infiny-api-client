const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/vendor.js', 'public/js')
    .js('resources/js/services.js', 'public/js')
    .js('resources/js/service-details.js', 'public/js')
    .version()
    .postCss('resources/css/vendor.css', 'public/css', [
        //
    ]);

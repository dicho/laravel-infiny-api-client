## Laravel Infiny API Client

A fresh Laravel v8.58.0 project with already installed [Infiny API Client Package v.1.0.0](https://gitlab.com/dicho/infiny-api-client).
It requires PHP 7.3-8.0.

Currently it can display a list of Infiny set vices and details about each of them:

```
{your Laravel project's URL}/services
{your Laravel project's URL}/service/{serviceId}/details
```

## Configure

Add your Infiny API credentials as environment variables in the .env file like:

```
INFINY_API_CLIENT_ID={client_id}
INFINY_API_CLIENT_SECRET={client_secret}
INFINY_API_ENDPOINT={endpoint} (API's base URL)
INFINY_API_VERSION={version} (e.g. INFINY_API_VERSION=1)

```
## License

Laravel Infiny API Client is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

<?php

use Illuminate\Support\Facades\Route;
use Banovs\InfinyApiClient\Request\ServicesRequest;
use Banovs\InfinyApiClient\Request\ServiceDetailsRequest;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('welcome');
});

Route::get('/request/services',
    function (ServicesRequest $servicesRequest) {
        return $servicesRequest->send();
    }
);

Route::get('/request/service/{serviceId}/details',
    function ($serviceId, ServiceDetailsRequest $serviceDetailsRequest) {
        return $serviceDetailsRequest->send($serviceId);
    }
);

Route::get('/services', function () {
    return view('services.index');
});

Route::get('/service/{serviceId}/details', function ($serviceId) {
    return view('services.details', [ 'serviceId' => $serviceId ]);
});


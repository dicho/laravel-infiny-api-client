new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    data() {
        return {
            search: '',
            items: [],
            loadTable: true
        };

    },
    computed: {
        headers () {
            return [
                {
                    text: 'ID',
                    align: 'start',
                    value: 'id',
                },
                {
                    text: 'Service Name',
                    value: 'name',
                },
                {text: 'Port Name', value: 'port.name'},
                {text: 'Service Status', value: 'status'},
                {
                    text: 'Action',
                    value: 'view_details_btn',
                    sortable: false
                },
            ]
        },
    },
    created: function () {
        this.getData();
    },
    methods: {
        async getData() {
            const response = await axios.get('/request/services');
            this.items = response.data.services;
            this.loadTable = false;
        },
        viewDetails(serviceId) {
            window.open('/service/' + serviceId + '/details', '_blank');
        }
    }
});
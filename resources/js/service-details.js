 new Vue({
     el: '#app',
     vuetify: new Vuetify(),
     data () {
         return {
             tab: null,
             infinyService: null,
             items: [
                 'General', 'Port', 'B Port', 'CSP Status', 'Statistics',
             ],
             tabContent: {},
         }
     },
     created: function () {
         this.getData();
     },
     methods: {
         async getData() {
             const response = await axios.get('/request/service/' + serviceId  + '/details');
             this.infinyService = response.data;
         },
         notEmptyObject(value){
             return Object.keys(value).length
         },
         isObject(value) {
             return typeof value === 'object' && value !== null;
         },
     }
 });
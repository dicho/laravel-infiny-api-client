@extends('layouts.main')

@section('title', 'Infiny Services')

@section('content')

        <div id="app">
            <v-app id="inspire">
                <v-card>
                    <v-card-title>
                        Infiny Services
                        <v-spacer></v-spacer>
                        <v-text-field
                          v-model="search"
                          append-icon="mdi-magnify"
                          label="Search"
                          single-line
                          hide-details
                        ></v-text-field>
                    </v-card-title>
                    <v-data-table
                     :headers="headers"
                     :items="items"
                     :search="search"
                     :loading="loadTable"
                     loading-text="Loading... Please wait"
                    >
                        <v-alert slot="no-results" :value="true" color="error" icon="warning">
                          Your search for "@{{ search }}" found no results.
                        </v-alert>
                        <template v-slot:item.port.name="{ item }">
                            @{{ item.port.name }}
                        </template>
                        <template v-slot:item.view_details_btn="{ item }">
                            <v-btn
                             class="ma-2"
                             color="primary"
                             dark
                             small
                             v-on:click="viewDetails(item.id)"
                            >
                                View Details
                                <v-icon dark right>mdi-eye-plus</v-icon>
                            </v-btn>
                        </template>
                    </v-data-table>
                </v-card>
            </v-app>
        </div>

        @section('scripts')

        @parent
        <script src="/js/services.js"></script>

        @endsection

@endsection
 

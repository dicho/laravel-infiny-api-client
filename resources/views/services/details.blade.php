@extends('layouts.main')

@section('title', 'Infiny Service Details')

@section('content')

        <div id="app">
            <v-app id="inspire">
                <template>
                    <v-card>
                        <v-toolbar color="primary" dark flat tile>
                            <v-toolbar-title>
                                Infiny Service: @{{ infinyService ? infinyService.name : ''}}
                            </v-toolbar-title>
                            <v-spacer></v-spacer>

                            <template v-slot:extension>
                                <v-tabs v-model="tab" align-with-title >
                                    <v-tabs-slider color="yellow"></v-tabs-slider>
                                        <v-tab v-for="item in items" :key="item">
                                            @{{ item }}
                                        </v-tab>
                                </v-tabs>
                            </template>
                        </v-toolbar>

                        <v-tabs-items v-model="tab">

                            <v-progress-linear indeterminate color="primary" v-if="!infinyService" class="mt-3"></v-progress-linear>

                            <v-tab-item v-for="item in items" :key="item">
                                <v-card flat>
                                    <v-card-text>

                                        <div v-if="infinyService">
                                            <div v-if="item == 'General'">
                                                <div v-for="(value, name) in infinyService" v-if="!isObject(value)">
                                                    <b>@{{ name }}</b>: @{{ value ? value : 'no' }}
                                                </div>
                                            </div>
                                            <div v-if="item == 'Port'">
                                                <div v-for="(value, name) in infinyService.port" v-if="!isObject(value)">
                                                    <b>@{{ name }}</b>: @{{ value ? value : 'no' }}
                                                </div>
                                                <div v-for="(value, name) in infinyService.port" v-if="isObject(value)">
                                                    <b>@{{ name }}</b>:
                                                        <ul>
                                                          <li v-for="(childValue, childName) in value" v-if="!isObject(childValue)">
                                                              @{{ childName }}: @{{ childValue ? childValue : 'no' }}
                                                          </li>
                                                          <li v-for="(childValue, childName) in value" v-if="isObject(childValue)">
                                                              @{{ childName }}:
                                                              <span v-for="(infantValue, infantName) in childValue" v-if="!isObject(infantValue)">
                                                                  @{{ infantName }}: @{{ infantValue ? infantValue : 'no' }}
                                                              </span>
                                                          </li>
                                                        </ul>
                                                </div>
                                            </div>
                                            <div v-if="item == 'B Port'">
                                             <div v-for="(value, name) in infinyService['b_port']" v-if="!isObject(value)">
                                                    <b>@{{ name }}</b>: @{{ value ? value : 'no' }}
                                                </div>
                                                <div v-for="(value, name) in infinyService.port" v-if="isObject(value)">
                                                    <b>@{{ name }}</b>:
                                                        <ul>
                                                          <li v-for="(childValue, childName) in value" v-if="!isObject(childValue)">
                                                              @{{ childName }}: @{{ childValue ? childValue : 'no' }}
                                                          </li>
                                                          <li v-for="(childValue, childName) in value" v-if="isObject(childValue)">
                                                              @{{ childName }}:
                                                              <span v-for="(infantValue, infantName) in childValue" v-if="!isObject(infantValue)">
                                                                  @{{ infantName }}: @{{ infantValue ? infantValue : 'no' }}
                                                              </span>
                                                          </li>
                                                        </ul>
                                                </div>
                                            </div>
                                            <div v-if="item == 'CSP Status'">
                                                <div v-for="(value, name) in infinyService.csp_status" v-if="!isObject(value)">
                                                    <b>@{{ name }}</b>: @{{ value ? value : 'no' }}
                                                </div>
                                            </div>
                                            <div v-if="item == 'Statistics'">
                                                <template>
                                                    <v-simple-table>
                                                        <template v-slot:default>
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-left">
                                                                        Key
                                                                    </th>
                                                                    <th class="text-left">
                                                                        Name
                                                                    </th>
                                                                    <th class="text-left">
                                                                        Last Value
                                                                    </th>
                                                                    <th class="text-left">
                                                                        Last Value Raw
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr
                                                                 v-for="(value, name) in infinyService.statistics"
                                                                 :key="name"
                                                                >
                                                                    <td>@{{ name }}</td>
                                                                    <td>@{{ value.name }}</td>
                                                                    <td>@{{ value.lastvalue }}</td>
                                                                    <td>@{{ value.lastvalue_raw }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </template>
                                                    </v-simple-table>
                                                </template>
                                            </div>
                                        </div>
                                    </v-card-text>
                                </v-card>
                            </v-tab-item>
                        </v-tabs-items>

                    </v-card>
                </template>
            </v-app>
        </div>

        @section('scripts')

        @parent
        <script>const serviceId = '{{ $serviceId }}'</script>
        <script src="/js/service-details.js"></script>

        @endsection

@endsection
 
